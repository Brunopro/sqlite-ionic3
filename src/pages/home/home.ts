import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController, ItemSliding, AlertOptions, Loading, Platform } from 'ionic-angular';

import { Movie } from '../../models/movie.model';
import { MovieProvider } from '../../providers/movie/movie';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  movies: Movie[] = []

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public movieProv: MovieProvider,
    public platform: Platform) {

    console.log('is cordova avaliable?', this.platform.is('cordova'));
  }

  ionViewDidLoad() {
    if (this.platform.is('cordova') == false) {
      console.log('criando array example case cordova not avaliable');
      this.movies = [
        new Movie('Interestelar'),
        new Movie('A origem')
      ];
    } else {
      
      this.movieProv.getAll().then((movies: Movie[]) => {
        console.log(`get all movies result ${movies}`);
        this.movies = movies;
      });
    }

  }




   onSave(type: string, item? :ItemSliding, movie?:Movie): void{
    let title: string = type.charAt(0).toUpperCase() + type.substr(1);
    this.showAlert({
      itemSliding: item,
      title: `${title} movie`,
      type: type,
      movie: movie
    });
  }


  private showAlert(options: { itemSliding?: ItemSliding, title: string, type: string, movie?: Movie }): void {
    let alertOptions: AlertOptions = {
      title: options.title,
      inputs: [
        {
          name: 'title',
          placeholder: 'Movie title'
        }
      ],
      buttons: [
        'Cancel',
        {
          text: 'Save',
          handler: (data) => {
            let loading: Loading = this.showLoading(`Saving ${data.title} movie...`);

            let contextMovie: Movie;

            switch (options.type) {
              case 'create':
                contextMovie = new Movie(data.title);
              break;
              case 'update':
                options.movie.title = data.title;
                contextMovie = options.movie;
              break;
            }

            this.movieProv[options.type](contextMovie)
              .then((result: any) => {
                console.log('O que me retornar o create ou update', result)
                if(options.type === 'create') this.movies.unshift(result);
                loading.dismiss();

                if(options.itemSliding) options.itemSliding.close();
              })
          }
        }
      ]
    };

    if (options.type === 'update'){
      alertOptions.inputs[0]['value'] = options.movie.title;
    }

    this.alertCtrl.create(alertOptions).present();
  }

  private showLoading(message?: string): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: message || 'Please wait...'
    });
    loading.present();
    return loading;
  }


  onUpdate(movie){
    console.log('onUpdate');
  }

  onDelete(movie: Movie){
    console.log('onDelete');

    this.alertCtrl.create({
      title: `Do you want to delete '${movie.title}' movie`,
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            let loading: Loading = this.showLoading(`Deleting ${movie.title} movie`);

            this.movieProv.delete(movie.id).then((deleted: boolean ) => {
              console.log('return delete', deleted)
              if (deleted) {
                this.movies.splice(this.movies.indexOf(movie), 1)
              }
              loading.dismiss();
            })
          }
        },
        'No'
      ]
    }).present();
  }

  onCreate(){
    console.log('onCreate');
  }

}
